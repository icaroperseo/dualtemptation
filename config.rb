require 'sass-globbing'

# Require any additional compass plugins here.
project_type = :stand_alone

# Publishing paths
http_path = "/dualtemptation/"
http_images_path = "/dualtemptation/images"
http_generated_images_path = "/dualtemptation/images"
http_fonts_path = "/dualtemptation/fonts"
css_dir = "public/dualtemptation/stylesheets"

# Local development paths
sass_dir = "sass"
images_dir = "source/images"
fonts_dir = "source/fonts"

line_comments = false
output_style = :compressed
