---
layout: page
title: "Acerca de"
date:
comments: false
sharing: false
footer: false
---
¡Bienvenido a **Dual Temptation**! Aquí encontrará información sobre
_GNU/Linux_, _Software Libre_, _How to's_, _Actualidad_ y _Artículos de opinión
personal_. Los temas aquí expuestos son desarrollados tomando siempre cómo base
la filosofía que envuelve a la _Cultura Libre_.

## Sobre el autor

**Iván Ruvalcaba** ([Icaro Perseo](https://identi.ca/icaroperseo)) es
estudiante de Ingeniería en Computación, Profesional Técnico en Informática y
Cofundador de **&lt;°DesdeLinux**.

Dentro de mis anteriores proyectos se encuentra el haber sido el titular de
**Perseo’s Blog**, así como también, haber participado brevemente dentro del
proyecto **Hackers & Developers**. Actualmente emprendo este nuevo proyecto con
la finalidad de compartir mis experiencias como eterno autodidacta.

## Licencia

El contenido de este sitio está protegido bajo la **Licencia de Documentación
Libre de GNU (GNU FDL)** — _a no ser que se indique explícitamente lo
contrario_ —.

> Permission is granted to copy, distribute and/or modify this document under
> the terms of the GNU Free Documentation License, Version 1.3 or any later
> version published by the Free Software Foundation; with the Invariant
> Sections: “Todo texto, gráfico u otro medio similar que exprese o manifieste
> mi opinión o sugerencia personal, ya sea de manera explícita o implícita”,
> one Front-Cover: “Dual Temptation, Software y Cultura Libre”, and with no
> Back-Cover Texts. A copy of the license is included in the section entitled
> “GNU Free Documentation License”.

{% img center images/blog/copyleft.jpg "copyleft" "copyleft Logo" %}

Si desea obtener más información sobre dicha licencia, puede recurrir al
siguiente enlace: [Licencia de Documentación Libre de GNU](https://www.gnu.org/licenses/#FDL).

{% img center images/blog/fsf.jpg 200 "Free Software Foundation" "FSF Logo" %}
