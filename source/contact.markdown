---
layout: page
title: "Contacto"
date:
comments: false
sharing: false
footer: false
---
## Comentarios y opiniones

Puede enviarlos a mi dirección de <a
href="mailto:&#100;&#117;&#97;&#108;&#116;&#101;&#109;&#112;&#116;&#97;&#116;&#105;&#111;&#110;&#64;&#111;&#112;&#101;&#110;&#109;&#97;&#105;&#108;&#98;&#111;&#120;&#46;&#111;&#114;&#103;">&#101;&#45;&#109;&#97;&#105;&#108;</a>,
así como también, puede seguirme en alguna de las siguientes redes sociales
libres: [Pump.io](https://mipump.es/icaroperseo) —preferentemente— y
[GNU Social](https://loadaverage.org/icaroperseo).

## Feedback

Si considera que la información de algún artículo necesita revisión, es
incorrecta, inadecuada o desea realizar un aporte al mismo, por favor,
[abra un reporte de error](https://gitlab.com/icaroperseo/dualtemptation/issues)
para que pueda darle el debido seguimiento. **Su contribución es muy valiosa**.

### ¿Por qué sólo es posible comentar a través de correo electrónico?

Para ser franco, podría conformar un extenso listado que incluya las diversas
situaciones por las que considero que incorporar un sistema de comentarios más
dinámico al sitio sería poco factible —por citar un único ejemplo, tendría que
recurrir a _Disqus_ ~~empresa que busca mantener cautiva a su comunidad con la
intención de mercadear la información de sus usuarios~~ o alguna otra ~~empresa
que busque mantener cautiva a su comunidad con la finalidad de mercadear la
información de sus usuarios~~ alternativa al mismo—, lo que, tanto para usted
como para mi, resultaría bastante abrumador.

> Por favor no me mal interprete, no pretendo ser _"quien"_ le dicte lo que
> puede, o no, hacer con su propia información.

**La verdadera intención que existe detrás de esta decisión es la de buscar
diversificar el intercambio de ideas, pensamientos, reflexiones y
experiencias**, no únicamente de la forma convencional, sino que también sirva
como factor determinante para la apertura o exploración de nuevos medios o vías
de comunicación —la creación de un blog o sitio web por ejemplo—.

Como usuarios de la web estamos muy acostumbrados a la relación _autor-lector_,
el primero expone abiertamente sus inquietudes u opiniones y el segundo
reacciona en consecuencia, obteniendo como resultado un efecto poco deseable:
delimitar considerablemente el _dialogo_. A priori, puede llegarse a suponer
que se fomenta el dialogo al hacer uso de los comentarios del sitio, pero en la
práctica esto solo es parcialmente cierto si el sitio en cuestión alberga una
amplia comunidad de visitantes activos y recurrentes. Por otra parte, el sitio
actúa indirectamente como _silo_, ya que para poder aportar una nueva opinión
sobre el trema tratado o para continuar el debate es necesario cohesionarse en
sumo grado con tal sitio web y su comunidad, limitando en el proceso otras vías
de expresión.

Para sintetizar —y finalizar al mismo tiempo este _"lapsus filosófico"_—, si
existe algo que le gustaría decir o sugerir le invito a iniciar el _dialogo_ a
través de mis redes sociales o mejor aún, inicie un nuevo blog o sitio web,
existen muchas aplicaciones y servicios para ello:

+ [Octopress](http://octopress.org/).
+ [Jekyll](https://jekyllrb.com/).
+ [Hugo](https://gohugo.io/).
+ [Hexo](https://hexo.io/).
+ [Nikola](https://getnikola.com/).
+ [Pelican](http://docs.getpelican.com/en/3.6.3/).
+ [Drupal](https://www.drupal.org/).
+ [Ghost](https://ghost.org/).
+ [Wordpress](https://es.wordpress.com/).

**Espero sus comentarios, ¡realmente me encantaría conocer su opinión al
respecto!**
